package com.sdj.tctest.shop.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import com.sdj.tctest.shop.domain.Discount;
import com.sdj.tctest.shop.domain.Item;
import com.sdj.tctest.shop.domain.Product;

public class BasketTest {

   private static final Product  BREAD      = new Product(2, "Bread", 195);
   private static final Discount DISCOUNT_1 = new Discount("Buy 1 get 1 free", 1, 99, null);
   private static final Discount DISCOUNT_2 = new Discount("Buy 3 get 1 free", 2, 49, null);
   private static final Discount DISCOUNT_3 = new Discount("Buy 2 get 10% off", 3, 5, null);
   private static final Product  MILK       = new Product(1, "Milk", 150);

   @Test
   public void testAddRemoveProducts() {

      Map<Product, Item> items = new HashMap<>();
      Set<Discount> discounts = new HashSet<>();

      Basket basket = new Basket(items, discounts);

      basket.putItem(MILK, 1);

      assertEquals(1, items.get(MILK).getQuantity());
      assertEquals(null, items.get(BREAD));
      assertEquals(1, items.size());

      basket.putItem(MILK, 5);
      basket.putItem(BREAD, 5);

      assertEquals(5, items.get(MILK).getQuantity());
      assertEquals(5, items.get(BREAD).getQuantity());
      assertEquals(2, items.size());

      basket.putItem(MILK, 7);
      basket.putItem(BREAD, 4);

      assertEquals(7, items.get(MILK).getQuantity());
      assertEquals(4, items.get(BREAD).getQuantity());
      assertEquals(2, items.size());

      basket.putItem(MILK, 0);
      basket.putItem(BREAD, 2);

      assertEquals(null, items.get(MILK));
      assertEquals(2, items.get(BREAD).getQuantity());
      assertEquals(1, items.size());

      basket.putItem(BREAD, -2);

      assertEquals(null, items.get(MILK));
      assertEquals(null, items.get(BREAD));
      assertEquals(0, items.size());
   }

   @Test
   public void testClear() {

      Map<Product, Item> items = new HashMap<>();
      Set<Discount> discounts = new HashSet<>();

      Basket basket = new Basket(items, discounts);
      Product milk = new Product(1, "Milk", 150);
      Product bread = new Product(2, "Bread", 195);

      basket.putItem(milk, 1);
      basket.putItem(bread, 1);
      basket.addDiscount(DISCOUNT_3);

      assertEquals(2, items.size());
      assertEquals(1, discounts.size());

      basket.clear();

      assertEquals(0, items.size());
      assertEquals(0, discounts.size());
   }

   @Test
   public void testDiscount() {

      Set<Discount> discounts = new HashSet<>();
      Basket basket = new Basket(null, discounts);

      assertEquals(0, discounts.size());

      basket.addDiscount(DISCOUNT_1);
      assertEquals(1, discounts.size());
      basket.addDiscount(DISCOUNT_2);
      assertEquals(2, discounts.size());

      for (Discount d : discounts) {
         switch (d.getId()) {
         case 1:
            assertEquals(DISCOUNT_1, d);
            break;
         case 2:
            assertEquals(DISCOUNT_2, d);
            break;
         default:
            fail();
         }
      }

      basket.clearDiscounts();
      assertEquals(0, discounts.size());
   }

   @Test
   public void testQuantities() {

      Map<Product, Item> items = new HashMap<>();
      Set<Discount> discounts = new HashSet<>();

      Basket basket = new Basket(items, discounts);

      basket.putItem(MILK, 5);
      basket.putItem(BREAD, 4, 3);

      assertEquals(5, (int) basket.getQuantity(MILK));
      assertEquals(0, (int) basket.getDiscountedQuantity(MILK));
      assertEquals(4, (int) basket.getQuantity(BREAD));
      assertEquals(3, (int) basket.getDiscountedQuantity(BREAD));

      basket.putItem(MILK, 0);
      assertEquals(0, (int) basket.getQuantity(MILK));
   }

   @Test
   public void testTotals() {

      Map<Product, Item> items = new HashMap<>();
      Set<Discount> discounts = new HashSet<>();

      Basket basket = new Basket(items, discounts);

      assertEquals(basket.getPenceTotal(), 0);
      assertEquals(basket.getFormattedTotal(), "£0.00");

      basket.putItem(MILK, 1);
      basket.putItem(BREAD, 1);

      assertEquals(basket.getPenceTotal(), 345);
      assertEquals(basket.getFormattedTotal(), "£3.45");

      basket.putItem(MILK, 2);

      assertEquals(basket.getPenceTotal(), 495);
      assertEquals(basket.getFormattedTotal(), "£4.95");

      basket.putItem(BREAD, 2);

      assertEquals(basket.getPenceTotal(), 690);
      assertEquals(basket.getFormattedTotal(), "£6.90");

      basket.addDiscount(DISCOUNT_1);

      assertEquals(basket.getPenceTotal(), 591);
      assertEquals(basket.getFormattedTotal(), "£5.91");

      basket.addDiscount(DISCOUNT_2);

      assertEquals(basket.getPenceTotal(), 542);
      assertEquals(basket.getFormattedTotal(), "£5.42");
   }

}
