package com.sdj.tctest.shop.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import com.sdj.tctest.shop.domain.Discount;
import com.sdj.tctest.shop.domain.Item;
import com.sdj.tctest.shop.domain.Product;

public class BuyNProdXGetPercentOffProdYTest {

   private static final Product BREAD = new Product(2, "Bread", 195);
   private static final Product MILK  = new Product(1, "Milk", 150);
   private static DiscountRule  RULE1 = new BuyNProdXGetPercentOffProdY("Buy 2 Bread get 10% off milk", BREAD, 2, MILK,
         10);
   private static DiscountRule  RULE2 = new BuyNProdXGetPercentOffProdY("Buy 3 Bread get 3rd free", BREAD, 2, BREAD,
         100);

   @Test
   public void testDiscountApplied() {

      Map<Product, Item> items = new HashMap<>();
      Set<Discount> discounts = new HashSet<>();

      Basket basket = new Basket(items, discounts);

      basket.putItem(MILK, 1);
      basket.putItem(BREAD, 2);

      Discount discount = RULE1.generateDiscount(basket);
      assertEquals(15, discount.getValuePence());

      basket.putItem(BREAD, 4);
      discount = RULE2.generateDiscount(basket);
      assertEquals(195, discount.getValuePence());

      basket.putItem(BREAD, 4, 1);
      discount = RULE2.generateDiscount(basket);
      assertEquals(195, discount.getValuePence());

   }

   @Test
   public void testDiscountNotApplied() {

      Map<Product, Item> items = new HashMap<>();
      Set<Discount> discounts = new HashSet<>();

      Basket basket = new Basket(items, discounts);

      basket.putItem(MILK, 1);
      basket.putItem(BREAD, 1);

      Discount discount = RULE1.generateDiscount(basket);
      assertNull(discount);

      basket.putItem(MILK, 2);
      basket.putItem(BREAD, 1);

      discount = RULE1.generateDiscount(basket);
      assertNull(discount);

      basket.putItem(BREAD, 2, 1);

      discount = RULE1.generateDiscount(basket);
      assertNull(discount);

      discount = RULE2.generateDiscount(basket);
      assertNull(discount);

      basket.putItem(BREAD, 4, 2);
      discount = RULE2.generateDiscount(basket);
      assertNull(discount);
   }

}
