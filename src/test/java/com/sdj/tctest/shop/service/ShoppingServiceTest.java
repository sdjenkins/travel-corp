package com.sdj.tctest.shop.service;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import com.sdj.tctest.shop.action.Basket;
import com.sdj.tctest.shop.action.BuyNProdXGetPercentOffProdY;
import com.sdj.tctest.shop.action.DiscountRule;
import com.sdj.tctest.shop.domain.Discount;
import com.sdj.tctest.shop.domain.Item;
import com.sdj.tctest.shop.domain.Product;

public class ShoppingServiceTest {

   private static final Product BREAD  = new Product(1, "Bread", 100);
   private static final Product BUTTER = new Product(2, "Butter", 80);
   private static final Product MILK   = new Product(3, "Milk", 115);
   private static DiscountRule  RULE1  = new BuyNProdXGetPercentOffProdY("Buy 2 butter get 50% off bread", BUTTER, 2,
         BREAD, 50);
   private static DiscountRule  RULE2  = new BuyNProdXGetPercentOffProdY("Buy 3 milk get 4th free", MILK, 3, MILK, 100);

   private static final ShoppingService service = new ShoppingService(
         new HashSet<>(Arrays.asList(new DiscountRule[] { RULE1, RULE2 })));

   @Test
   public void testBasicBasket() {

      Map<Product, Item> items = new HashMap<>();
      Set<Discount> discounts = new HashSet<>();

      Basket basket = new Basket(items, discounts);

      service.putProductInBasket(basket, MILK, 1);
      assertEquals(1, basket.getQuantity(MILK));
      assertEquals("£1.15", basket.getFormattedTotal());

      service.putProductInBasket(basket, MILK, 2);
      assertEquals(2, basket.getQuantity(MILK));
      assertEquals("£2.30", basket.getFormattedTotal());

      service.putProductInBasket(basket, BREAD, 1);
      assertEquals(1, basket.getQuantity(BREAD));
      assertEquals("£3.30", basket.getFormattedTotal());

      service.putProductInBasket(basket, BUTTER, 1);
      assertEquals(1, basket.getQuantity(BUTTER));
      assertEquals("£4.10", basket.getFormattedTotal());
   }

   @Test
   public void testBasketSenarion1() {

      Map<Product, Item> items = new HashMap<>();
      Set<Discount> discounts = new HashSet<>();

      Basket basket = new Basket(items, discounts);

      service.putProductInBasket(basket, BREAD, 1);
      service.putProductInBasket(basket, BUTTER, 1);
      service.putProductInBasket(basket, MILK, 1);

      assertEquals("£2.95", basket.getFormattedTotal());
   }

   @Test
   public void testBasketSenarion2() {

      Map<Product, Item> items = new HashMap<>();
      Set<Discount> discounts = new HashSet<>();

      Basket basket = new Basket(items, discounts);

      service.putProductInBasket(basket, BUTTER, 2);
      service.putProductInBasket(basket, BREAD, 2);

      assertEquals("£3.10", basket.getFormattedTotal());
   }

   @Test
   public void testBasketSenarion3() {

      Map<Product, Item> items = new HashMap<>();
      Set<Discount> discounts = new HashSet<>();

      Basket basket = new Basket(items, discounts);

      service.putProductInBasket(basket, MILK, 4);

      assertEquals("£3.45", basket.getFormattedTotal());
   }

   @Test
   public void testBasketSenarion4() {

      Map<Product, Item> items = new HashMap<>();
      Set<Discount> discounts = new HashSet<>();

      Basket basket = new Basket(items, discounts);

      service.putProductInBasket(basket, BUTTER, 2);
      service.putProductInBasket(basket, BREAD, 1);
      service.putProductInBasket(basket, MILK, 8);

      assertEquals("£9.00", basket.getFormattedTotal());
   }

}
