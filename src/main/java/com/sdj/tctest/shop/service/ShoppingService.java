package com.sdj.tctest.shop.service;

import java.util.Map.Entry;
import java.util.Set;

import com.sdj.tctest.shop.action.Basket;
import com.sdj.tctest.shop.action.DiscountRule;
import com.sdj.tctest.shop.domain.Discount;
import com.sdj.tctest.shop.domain.Product;

public class ShoppingService {

   private final Set<DiscountRule> discountRules;

   public ShoppingService(Set<DiscountRule> discountRules) {
      super();
      this.discountRules = discountRules;
   }

   private void applyDiscount(DiscountRule rule, Basket basket) {

      Discount discount = null;
      while ((discount = rule.generateDiscount(basket)) != null) {
         basket.addDiscount(discount);

         // Mark products as having had discount applied already
         for (Entry<Product, Integer> item : discount.getQualifyingItems().entrySet()) {
            Product product = item.getKey();
            int quantity = basket.getQuantity(product);
            int noAffected = item.getValue();
            int discounted = basket.getDiscountedQuantity(product) + noAffected;
            basket.putItem(product, quantity, discounted);
         }
      }
   }

   public void putProductInBasket(Basket basket, Product product, int quantity) {

      basket.putItem(product, quantity);
      basket.clearDiscounts();
      for (DiscountRule rule : discountRules) {
         applyDiscount(rule, basket);
      }
   }

}
