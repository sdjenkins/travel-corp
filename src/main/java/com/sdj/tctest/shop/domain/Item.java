package com.sdj.tctest.shop.domain;

public class Item {

   private final int discountedQuantity;
   private final int quantity;

   public Item(int quantity) {

      this(quantity, 0);
   }

   public Item(int quantity, int discountedQuantity) {

      this.quantity = quantity;
      this.discountedQuantity = discountedQuantity;
   }

   public int getDiscountedQuantity() {

      return discountedQuantity;
   }

   public int getQuantity() {

      return quantity;
   }
}
