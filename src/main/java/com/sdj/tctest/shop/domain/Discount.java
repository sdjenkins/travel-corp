package com.sdj.tctest.shop.domain;

import java.util.Map;

public class Discount {

   private final String                description;
   private final int                   id;
   private final Map<Product, Integer> qualifyingItems;
   private final int                   valuePence;

   public Discount(String description, int id, int valuePence, Map<Product, Integer> qualifyingItems) {

      super();
      this.description = description;
      this.id = id;
      this.valuePence = valuePence;
      this.qualifyingItems = qualifyingItems;
   }

   @Override
   public boolean equals(Object obj) {

      if (this == obj)
         return true;
      if (obj == null)
         return false;
      if (getClass() != obj.getClass())
         return false;
      Discount other = (Discount) obj;
      if (id != other.id)
         return false;
      return true;
   }

   public String getDescription() {

      return description;
   }

   public int getId() {

      return id;
   }

   public Map<Product, Integer> getQualifyingItems() {

      return qualifyingItems;
   }

   public int getValuePence() {

      return valuePence;
   }

   @Override
   public int hashCode() {

      final int prime = 31;
      int result = 1;
      result = prime * result + id;
      return result;
   }
}
