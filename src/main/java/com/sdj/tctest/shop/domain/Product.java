package com.sdj.tctest.shop.domain;

public class Product {

   private final int    id;
   private final String name;
   private final int    pricePence;

   public Product(int id, String name, int pricePence) {

      super();
      this.id = id;
      this.name = name;
      this.pricePence = pricePence;
   }

   @Override
   public boolean equals(Object obj) {

      if (this == obj)
         return true;
      if (obj == null)
         return false;
      if (getClass() != obj.getClass())
         return false;
      Product other = (Product) obj;
      if (id != other.id)
         return false;
      return true;
   }

   public int getId() {

      return id;
   }

   public String getName() {

      return name;
   }

   public int getPricePence() {

      return pricePence;
   }

   @Override
   public int hashCode() {

      final int prime = 31;
      int result = 1;
      result = prime * result + id;
      return result;
   }
}
