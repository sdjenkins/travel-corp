package com.sdj.tctest.shop.action;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.sdj.tctest.shop.domain.Discount;
import com.sdj.tctest.shop.domain.Product;

/**
 * Generic rule for buy n products and get a percentage discount off another (or
 * the same) product.
 *
 */
public class BuyNProdXGetPercentOffProdY implements DiscountRule {

   private static int                  nextId          = 0;
   private final int                   amountOff;
   private final String                description;
   private final Map<Product, Integer> qualifyingItems = new HashMap<>();

   public BuyNProdXGetPercentOffProdY(String description, Product prodX, int prodXQty, Product prodY,
         int percentOffProdY) {

      this.description = description;
      this.amountOff = prodY.getPricePence() * percentOffProdY / 100;

      // Check if X and Y are the same product.
      if (prodX.equals(prodY)) {
         this.qualifyingItems.put(prodX, prodXQty + 1);
      } else {
         this.qualifyingItems.put(prodX, prodXQty);
         this.qualifyingItems.put(prodY, 1);
      }
   }

   /**
    * Check that the basket has all the products and quantities required.
    *
    * @param basket
    * @return
    */
   private boolean itemDoesNotQualify(Entry<Product, Integer> entry, Basket basket) {

      Product product = entry.getKey();
      int available = basket.getQuantity(product) - basket.getDiscountedQuantity(product);
      if (available < entry.getValue()) {
         return true;
      }
      return false;
   }

   @Override
   public Discount generateDiscount(Basket basket) {

      for (Entry<Product, Integer> entry : qualifyingItems.entrySet()) {
         if (itemDoesNotQualify(entry, basket)) {
            return null;
         }
      }
      return new Discount(description, ++nextId, amountOff, qualifyingItems);
   }

}
