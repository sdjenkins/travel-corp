package com.sdj.tctest.shop.action;

import com.sdj.tctest.shop.domain.Discount;

public interface DiscountRule {

   Discount generateDiscount(Basket basket);
}
