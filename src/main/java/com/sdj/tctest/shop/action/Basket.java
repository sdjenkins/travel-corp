package com.sdj.tctest.shop.action;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.sdj.tctest.shop.domain.Discount;
import com.sdj.tctest.shop.domain.Item;
import com.sdj.tctest.shop.domain.Product;

public class Basket {

   private final Set<Discount>      discounts;
   private final Map<Product, Item> items;

   public Basket(Map<Product, Item> map, Set<Discount> set) {

      this.items = map;
      this.discounts = set;
   }

   public void addDiscount(Discount discount) {

      discounts.add(discount);
   }

   public void clear() {

      items.clear();
      clearDiscounts();
   }

   public void clearDiscounts() {

      discounts.clear();
      // reset the applied totals
      Map<Product, Item> tempMap = new HashMap<>();
      tempMap.putAll(items);
      items.clear();
      tempMap.forEach((p, i) -> items.put(p, new Item(i.getQuantity())));
   }

   public int getDiscountedQuantity(Product product) {

      Item item = items.get(product);
      if (item == null) {
         return 0;
      }
      return item.getDiscountedQuantity();
   }

   public String getFormattedTotal() {

      int total = getPenceTotal();
      int pounds = total / 100;
      // Add 100 to the pence to preserve two digits. Then remove the leading
      // '1'.
      String pence = ((100 + (total % 100)) + "").replaceFirst("1", ".");
      return "£" + pounds + pence;
   }

   public int getPenceTotal() {

      int subTotal = items.entrySet().stream().mapToInt(e -> e.getKey().getPricePence() * e.getValue().getQuantity())
            .sum();
      int discountTotal = discounts.stream().mapToInt(Discount::getValuePence).sum();

      return subTotal - discountTotal;
   }

   public int getQuantity(Product product) {

      Item item = items.get(product);
      if (item == null) {
         return 0;
      }
      return item.getQuantity();
   }

   public void putItem(Product product, int quantity) {

      items.remove(product);
      if (quantity < 1) {
         return;
      }
      items.put(product, new Item(quantity));
   }

   public void putItem(Product product, int quantity, int discountedQuantity) {

      items.remove(product);
      if (quantity < 1) {
         return;
      }
      items.put(product, new Item(quantity, discountedQuantity));
   }
}
